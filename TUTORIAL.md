# Decentralized Camp Workshop 3

We will be using [Etherscan](https://etherscan.io) for this tutorial.


## A further look at the _Ethereum_ blockchain

Blockchain technology has been the base for enabling cryptocurrencies, working in a decentralized and transparent way. There is a feeling that blockchain transactions are anonymous, but there is a difference between transparency and anonymity.

As seen in 2013, after the _Federal Bureau of Investigation_ (FBI) shutdown an online black market called _Silk Road_, which used _Bitcoin_ as payment method. Even though complete identities on a given blockchain are not entirely disclosed, every peer on the network is identified by an address and all its transactions can be followed and backtracked.

On a decentralized financial system, there are no banking institutions or the government.
However, some services work on top of blockchain to connect peers with one other or to serve a particular offering, such as an exchange, for trading, or online retailers, for buying and selling goods. Those services eventually link a cryptographic address to an identity, or proxy thereof, in the real world.

Similar to any other currency, cryptocurrencies are just another tool for financing acts. There is an inherent appeal with cryptographic transactions because they leverage on asymmetrical encryption. However, the forensic trail of a publicly available decentralized ledger can be more than enough for at least starting the process of linking digital transactions to specific peers on the network.

Different approaches to transactions can possibly be used, such as relying multiple peers, splitting up transactions and mixing up senders. Regardless of the approach to digital assets storage on a public decentralized network (cryptocurrencies, coins, tokens), they can all be followed back in one way or another.


### Inspecting the main network with _Etherscan_

_Etherscan_ is a block explorer and analytics platform for _Ethereum_.

It is possible to inspect accounts, transactions, smart contracts and every other relevant data stated in the _Ethereum_ network.


#### Inspecting addresses: is it a contract or a wallet ?

Wallets are also called normal or externally owned accounts.

Normal accounts are addresses that have an owner, the one who controls that particular account (by holding the respective private key).

The other type of account is a contract account, which may or may not be controlled by one or more normal accounts. Contracts have code attached to it, hence the name Smart Contracts, since they can carry not only data but also logic.


##### List of accounts

The lists only show the first 10000 entries, sorted by the total balance.

* Normal accounts: https://etherscan.io/accounts/a
* Contract accounts: https://etherscan.io/accounts/c

By clicking on an address, it is possible to inspect all details relevant to that account: balance and the history of all transactions, including tokens.

For contract accounts, there is also information about internal transactions (covered on the Inspecting Transactions section of this tutorial) and the contract itself, provided the source code has been made available.


##### List of contracts with source code

A complete list of contract accounts that have their source code available can be found here:  
https://etherscan.io/contractsVerified


##### List of tokens

A complete list of tokens sorted by Market Cap:  
https://etherscan.io/tokens


#### Inspecting transactions

It all starts with an account. Every account starts with a balance of 0 _Ether_.
In order to send a transaction, that address will need funds from some other address. It is not possible to know who owns those accounts, but all history can be backtracked.

There are two types of transactions that can be analyzed: regular transactions and internal contract transactions.


##### Regular transactions

Regular transactions are actual transactions, as defined by _Ethereum_ specification.  
Examples of transactions can be seen in real time on the following link:  
https://etherscan.io/txs

By clicking on any entry in the _TxHash_ column, it is possible to read all the details on that particular transaction, including who sent that transaction and who received it. That is called the transaction receipt.

Transactions that are set to pending can be observed on a different section:  
https://etherscan.io/txsPending


##### Internal transactions

Internal transactions are contract function calls, also referred to as messages. Calls usually have side effects, one of them when it includes a value in the process (_Ether_).

Given that, internal transactions are not stored on the blockchain and they are consequence of a regular transaction. 
Transfers are not included in the block as a regular transaction, thus they do not generate a transaction receipt. Every transfer is a message call but not every call is necessarily a transfer (of _Ether_).

Internal transactions can be inspected on _Etherscan_:
https://etherscan.io/txsInternal

The _ParentTxHash_ column corresponds to the actual transaction receipt that generated that call.


#### Inspecting blocks

_Etherscan_ offers a way to follow the mining process on the Blocks section:
https://etherscan.io/blocks

By clicking on entries in the _Height_ column and _Miner_ column, it is possible to inspect the block and the account who mined it, respectively.

It is possible to read all blocks, including the very first one:  
https://etherscan.io/block/1


##### Realtime statistics

Another useful tool with a better presentation and real time block and gas statistics is _Ethstats_:
https://ethstats.net


#### More data and charts

For more interesting data, there is the Charts section which aggregates all the most relevant macro information about the _Ethereum_ network:  
https://etherscan.io/charts


/TUTORIAL
